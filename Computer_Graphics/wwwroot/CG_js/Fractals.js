﻿function Complex(re, im) {
    this.re = re;
    this.im = im;
}

Complex.prototype.add = function (other) {
    return new Complex(this.re + other.re, this.im + other.im);
}

Complex.prototype.mul = function (other) {
    return new Complex(this.re * other.re - this.im * other.im,
        this.re * other.im + this.im * other.re);
}

Complex.prototype.abs = function () {
    return Math.sqrt(this.re * this.re + this.im * this.im);
}

function complex_cosh_ch(z) {
    return new Complex(
        Math.cos(z.im) * Math.sinh(z.re),
        Math.sin(z.im) * Math.cosh(z.re)
    );
}

function complex_sin(z) {
    return new Complex(
        Math.sin(z.re) * Math.cosh(z.im),
        Math.cos(z.re) * Math.sinh(z.im)
    );
}

async function belongs(re, im, iterations, choose) {
    let z = new Complex(re, im);
    let c = new Complex(1, 0);
    let i = 0;

    while (z.abs() < 15 && i < (iterations)) {
        if (choose.options[0].selected) {
            z = complex_cosh_ch(z);
            z = z.mul(z);
            z = z.mul(c);
        }
        else if (choose.options[1].selected) {
            let sincos = complex_sin(z.mul(new Complex(2, 0))).mul(new Complex(1 / 2, 0));
            z = sincos.mul(c);
        }
        i++;
    }
    return i;
}

let choose = document.getElementById('fracral_choose');
let scale = document.getElementById('Scale');
let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
function pixel(x, y, color) {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, 1, 1);
}

let colors = [];
async function draw(width, heigth, maxIterations, _scale, _choose, RGB) {

    let minRe = _scale * -1, maxRe = _scale, minIm = _scale * -1, maxIm = _scale;
    let reStep = (maxRe - minRe) / width, imStep = (maxIm - minIm) / heigth;
    let re = minRe;
    while (re < maxRe) {
        let im = minIm;
        while (im < maxIm) {
            let result = await belongs(re, im, maxIterations, _choose);
            let x = (re - minRe) / reStep, y = (im - minIm) / imStep;
            if (result == maxIterations) {
                pixel(x, y, 'black');
            } else {
                //
                const redValue = RGB[0];
                const greenValue = RGB[1];
                const blueValue = RGB[2];


                let hsl = RGBToHSL(redValue, greenValue, blueValue);
                //

                //let h = (hsl[0] - 15) + Math.round(hsl[0] * result * 1.0 / maxIterations);
                let h = (hsl[0] - 25) + Math.round(result / maxIterations);
                let color = 'hsl(' + hsl[0] + ', 100%, 50%)';

                let colorRGB = `rgb(${RGB[0]}, ${RGB[1]}, ${RGB[2]})`;

                pixel(x, y, colorRGB);
            }
            im += imStep;
        }
        re += reStep;
    }
}

let startBtn = document.getElementById('start');
startBtn.addEventListener('click', async () => {

    let RGB = [redSlider.value, greenSlider.value, blueSlider.value];

    let sc = scale.value;
    if (sc == '') {
        //alert("Enter scale!");
        //alert('Alert this pages');
        document.getElementById('popup1').style.zIndex = 100;
        document.getElementById('popup1').style.visibility = 'visible';
        return;
    }

    if (sc.includes(',')) {
        sc = sc.replace(',', '.');
    }

    if (choose.options[2].selected) {
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        let hsl = RGBToHSL(RGB[0], RGB[1], RGB[2]);
        //let color = 'hsl(' + hsl[0] + ', 100%, 50%)';
        let color = `rgb(${RGB[0]}, ${RGB[1]}, ${RGB[2]})`;



        draw_X(new Complex(0, 0), canvas.width, canvas.height, 15 * (1 / sc), color);

    }
    else {
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        let ch = choose;

        document.querySelector("#loader").style.display = "block";

        //draw(900, 600, 100);
        let iterations = [5, 10, 15, 25, 50, 75, 100];
        let i = 0;
        let interval = setInterval(async function () {
            console.log(iterations[i]);
            draw(canvas.width, canvas.height, iterations[i], sc, ch, RGB);
            i++;
            if (i >= iterations.length) {
                clearInterval(interval);
                document.querySelector("#loader").style.display = "none";

            }
        }, 500);
    }
});

function draw_X(start, width, height, lineWidth, color) {
    let limit = 29
    if (width < limit || height < limit) {
        return;
    }

    line(start, new Complex(start.re + width,
        start.im + height), lineWidth, color);
    line(new Complex(start.re + width, start.im),
        new Complex(start.re, start.im + height), lineWidth, color);

    const center = new Complex(start.re + Math.trunc(width / 2),
        start.im + Math.trunc(height / 2));

    let lineWidthNext = lineWidth - 3;

    let start1 = start;
    let width1 = width / 2;
    let height1 = height / 2;

    let start2 = new Complex(center.re, start.im);
    let width2 = width - width1;
    let height2 = height - height1;

    let start3 = new Complex(start.re, center.im);
    let width3 = width / 2;
    let height3 = height / 2;

    let start4 = center;
    let width4 = width - width3;
    let height4 = height - height3;

    draw_X(start1, width1, height1, lineWidthNext, color);
    draw_X(start2, width2, height2, lineWidthNext, color);
    draw_X(start3, width3, height3, lineWidthNext, color);
    draw_X(start4, width4, height4, lineWidthNext, color);
}

function line(start, end, lineWidth, color) {
    let width = end.re;
    let heigth = end.im;

    let center = new Complex(
        (width - start.re) / 2 + start.re,
        (heigth - start.im) / 2 + start.im
    )

    ctx.beginPath();

    ctx.moveTo(start.re, start.im);

    if (start.re < end.re && start.im < end.im) {
        //basic
        ctx.lineTo(center.re + lineWidth * 2 + 1, center.im);
        ctx.lineTo(center.re, center.im + lineWidth * 2 + 1);
    }
    else {
        //revert
        ctx.lineTo(center.re + lineWidth * 2 + 1, center.im);
        ctx.lineTo(center.re, center.im - lineWidth * 2 - 1);
    }

    ctx.fillStyle = color;
    ctx.fill();

    ctx.moveTo(end.re, end.im);
    if (start.re < end.re && start.im < end.im) {
        //basic
        ctx.lineTo(center.re + lineWidth * 2 + 1, center.im);
        ctx.lineTo(center.re, center.im + lineWidth * 2 + 1);
    }
    else {
        //revert
        ctx.lineTo(center.re + lineWidth * 2 + 1, center.im);
        ctx.lineTo(center.re, center.im - lineWidth * 2 - 1);
    }

    ctx.fillStyle = color;
    ctx.fill();





    let coef = 1;

    ctx.moveTo(center.re, center.im);
    ctx.lineTo(center.re - lineWidth * coef, start.im);
    ctx.lineTo(center.re + lineWidth * coef, start.im);

    ctx.fillStyle = color;
    ctx.fill();


    ctx.moveTo(center.re, center.im);
    ctx.lineTo(end.re, center.im + lineWidth * coef)
    ctx.lineTo(end.re, center.im - lineWidth * coef)

    ctx.fillStyle = color;
    ctx.fill();


    ctx.moveTo(center.re, center.im);
    ctx.lineTo(center.re - lineWidth * coef, end.im);
    ctx.lineTo(center.re + lineWidth * coef, end.im);

    ctx.fillStyle = color;
    ctx.fill();


    ctx.moveTo(center.re, center.im);
    ctx.lineTo(start.re, center.im + lineWidth * coef)
    ctx.lineTo(start.re, center.im - lineWidth * coef)


    ctx.fillStyle = color;
    ctx.fill();
    //ctx.stroke();
}

function line_old(start, end, lineWidth) {
    let width = end.re;
    let heigth = end.im;

    let maxWidth = (start.re > end.re ? start.re : end.re) + 1;
    let maxHeigth = (start.im > end.im ? start.im : end.im) + 1;

    let minWidth = (start.re < end.re ? start.re : end.re) - 1;
    let minHeight = (start.im < end.im ? start.im : end.im) - 1;

    ctx.beginPath();

    ctx.moveTo(start.re, start.im);
    ctx.lineTo(
        start.re + lineWidth < maxWidth ?
            start.re + lineWidth : start.re - lineWidth,
        start.im);
    ctx.lineTo(start.re,
        start.im + lineWidth < maxHeigth ?
            start.im + lineWidth : start.im - lineWidth);

    ctx.moveTo(
        start.re + lineWidth < maxWidth ?
            start.re + lineWidth : start.re - lineWidth,
        start.im);
    ctx.lineTo(width,
        heigth - lineWidth > minHeight ?
            heigth - lineWidth : heigth + lineWidth);
    ctx.lineTo(
        width - lineWidth > minWidth ?
            width - lineWidth : width + lineWidth,
        heigth);

    ctx.moveTo(width, heigth);
    ctx.lineTo(width,
        heigth - lineWidth > minHeight ?
            heigth - lineWidth : heigth + lineWidth);
    ctx.lineTo(width - lineWidth > minWidth ?
        width - lineWidth : width + lineWidth,
        heigth);

    ctx.moveTo(
        width - lineWidth > minWidth ?
            width - lineWidth : width + lineWidth,
        heigth);
    ctx.lineTo(start.re,
        start.im + lineWidth < maxHeigth ?
            start.im + lineWidth : start.im - lineWidth);
    ctx.lineTo(
        start.re + lineWidth < maxWidth ?
            start.re + lineWidth : start.re - lineWidth,
        start.im);

    ctx.fillStyle = 'white';
    ctx.fill();
}









const redSlider = document.getElementById('red');
const greenSlider = document.getElementById('green');
const blueSlider = document.getElementById('blue');
const colorCircle = document.getElementById('color-circle');
const colorBox = document.getElementById('color-box');

function updateColor() {
    const redValue = redSlider.value;
    const greenValue = greenSlider.value;
    const blueValue = blueSlider.value;

    const color = `rgb(${redValue}, ${greenValue}, ${blueValue})`;
    colorCircle.style.backgroundColor = color;
    //colorBox.style.backgroundColor = color;
}

redSlider.addEventListener('input', updateColor);
greenSlider.addEventListener('input', updateColor);
blueSlider.addEventListener('input', updateColor);
redSlider.value = 60;
greenSlider.value = 60;
blueSlider.value = 60;

updateColor(); // Встановлення початкового кольору

//chat
function rgbToHsl(r, g, b) {
    r /= 255;
    g /= 255;
    b /= 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    let h, s, l = (max + min) / 2;

    if (max === min) {
        h = s = 0; // achromatic
    } else {
        const d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }

        h /= 6;
    }

    return [h, s, l];
}

const RGBToHSL = (r, g, b) => {
    r /= 255;
    g /= 255;
    b /= 255;
    const l = Math.max(r, g, b);
    const s = l - Math.min(r, g, b);
    const h = s
        ? l === r
            ? (g - b) / s
            : l === g
                ? 2 + (b - r) / s
                : 4 + (r - g) / s
        : 0;
    return [
        60 * h < 0 ? 60 * h + 360 : 60 * h,
        100 * (s ? (l <= 0.5 ? s / (2 * l - s) : s / (2 - (2 * l - s))) : 0),
        (100 * (2 * l - s)) / 2,
    ];
};






function CloseAlert() {
    document.getElementById('popup1').style.zIndex = -1;
    document.getElementById('popup1').style.visibility = 'hidden';
}















/*var ALERT_TITLE = "Oops!";
var ALERT_BUTTON_TEXT = "Ok";

if (document.getElementById) {
    window.alert = function (txt) {
        createCustomAlert(txt);
    }
}

function createCustomAlert(txt) {
    d = document;

    if (d.getElementById("modalContainer")) return;

    mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
    mObj.id = "modalContainer";
    mObj.style.height = d.documentElement.scrollHeight + "px";

    alertObj = mObj.appendChild(d.createElement("div"));
    alertObj.id = "alertBox";
    if (d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
    alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
    alertObj.style.visiblity = "visible";

    h1 = alertObj.appendChild(d.createElement("h1"));
    h1.appendChild(d.createTextNode(ALERT_TITLE));

    msg = alertObj.appendChild(d.createElement("p"));
    //msg.appendChild(d.createTextNode(txt));
    msg.innerHTML = txt;

    btn = alertObj.appendChild(d.createElement("a"));
    btn.id = "closeBtn";
    btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
    btn.href = "#";
    btn.focus();
    btn.onclick = function () { removeCustomAlert(); return false; }

    alertObj.style.display = "block";

}

function removeCustomAlert() {
    document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}
function ful() {
    alert('Alert this pages');
}*/
