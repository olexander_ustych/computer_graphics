using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Computer_Graphics.Pages
{
    public class IndexModel_2 : PageModel
    {
        private readonly ILogger<IndexModel_2> _logger;

        public IndexModel_2(ILogger<IndexModel_2> logger)
        {
            _logger = logger;
        }

        public IActionResult OnPostHome()
        {
            return RedirectToPage("./Index");
        }

        public void OnGet()
        {

        }
    }
}
