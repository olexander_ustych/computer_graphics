const fileInput = document.getElementById('fileInput');
const previewImage = document.getElementById('previewImage');

const divInputImg = document.querySelectorAll('.imgContainer')[0];
const upload = document.getElementById('upload');

fileInput.addEventListener('change', function () {
    const selectedFile = fileInput.files[0];

    if (selectedFile) {
        // Display a preview of the selected image
        const reader = new FileReader();
        reader.onload = function (e) {
            previewImage.src = e.target.result;
            //previewImage.style.display = 'block';
        };
        reader.readAsDataURL(selectedFile);
    }   
});

let canvas = document.getElementById('canvas');
const outputImage = document.getElementById('outputImage');
let context = canvas.getContext('2d', {
    willReadFrequently: true,
});

upload.addEventListener('click', () => {    
    fileInput.click();    
    context.clearRect(0, 0, canvas.width, canvas.height);
});


//function ChangeColor() {
previewImage.onload = function () {
    /*context.clearRect(0, 0, canvas.width, canvas.height);
    canvas.width = 0;    
    canvas.height = 0;
    canvas.style.display = 'none';*/

    /*let parent = outputImage.parentElement;
    parent.removeChild(canvas);
    let _canvas = document.createElement('canvas');
    parent.appendChild(_canvas);
    let _context = canvas.getContext('2d');

    canvas = _canvas;
    context = _context;*/

    // Set the canvas size to match the image
    canvas.width = previewImage.width;
    canvas.height = previewImage.height;

    //context.clearRect(0, 0, canvas.width, canvas.height);
    // Draw the original image onto the canvas
    context.drawImage(previewImage, 0, 0);

    // Get the image data from the canvas
    const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
    const data = imageData.data;
    //console.log(data);

    // get hsl, then to rgb
    let hsl = [
        document.getElementById('h').value,
        document.getElementById('s').value,
        document.getElementById('l').value
    ];

    const [newR, newG, newB] = HSLToRGB(hsl[0], hsl[1], hsl[2]);    

    // Modify the image data
    for (let i = 0; i < data.length; i += 4) {
        const r = data[i];
        const g = data[i + 1];
        const b = data[i + 2];        

        let [_newR, _newG, _newB] = [];
        /**if(r == 219 && g == 195 && b == 0)
        {
            console.log("Here");
        }*/
        if ((r >= 150 && r <= 255) && (g >= 130 && g <= 255) && (b >= 0 && b <= 150)) {
            // Pixel falls within the defined yellow boundaries
            // Perform the desired action on these pixels

            let [h, s, l] = RGBToHSL(r, g, b);
            s = 100;
            [_newR, _newG, _newB] = HSLToRGB(h, s, l);

            data[i] = _newR;
            data[i + 1] = _newG;
            data[i + 2] = _newB;            
        }        
    }

    // Put the modified image data back on the canvas
    context.putImageData(imageData, 0, 0);
    console.log("After Here");

    // Display the modified image
    console.log("After Here 2");
    outputImage.src = canvas.toDataURL();
    
    DefineSize();
};


function DefineSize() {

    previewImage.style.maxWidth = getComputedStyle(divInputImg).width;
    previewImage.style.maxHeight = getComputedStyle(divInputImg).height;
    previewImage.style.minWidth = previewImage.style.maxWidth;
    previewImage.style.minHeight = previewImage.style.maxHeight;
    previewImage.style.borderRadius = getComputedStyle(divInputImg).borderRadius;

    previewImage.style.display = 'block';

    outputImage.style.maxWidth = getComputedStyle(divInputImg).width;
    outputImage.style.maxHeight = getComputedStyle(divInputImg).height;
    outputImage.style.minWidth = outputImage.style.maxWidth;
    outputImage.style.minHeight = outputImage.style.maxHeight;
    outputImage.style.borderRadius = getComputedStyle(divInputImg).borderRadius;

    outputImage.style.display = 'block';

    previewImage.addEventListener('click', CheckColorHSLforFirst);    
    outputImage.addEventListener('click', CheckColorHSL);
}


const HSLToRGB = (h, s, l) => {
    s /= 100;
    l /= 100;
    const k = n => (n + h / 30) % 12;
    const a = s * Math.min(l, 1 - l);
    const f = n =>
        l - a * Math.max(-1, Math.min(k(n) - 3, Math.min(9 - k(n), 1)));
    return [255 * f(0), 255 * f(8), 255 * f(4)];
};

function RGBToHSL(r, g, b) {
    r /= 255; g /= 255; b /= 255;
    let max = Math.max(r, g, b);
    let min = Math.min(r, g, b);
    let d = max - min;
    let h;
    if (d === 0) h = 0;
    else if (max === r) h = (g - b) / d % 6;
    else if (max === g) h = (b - r) / d + 2;
    else if (max === b) h = (r - g) / d + 4;
    let l = (min + max) / 2;
    let s = d === 0 ? 0 : d / (1 - Math.abs(2 * l - 1));
    return [h * 60, s * 100, l * 100];
  }



function CheckColorHSL(event) {
    //console.log(event.target.id);    
    const coordinates = outputImage.getBoundingClientRect();
    
    /*console.log("Координати: " + event.clientX + ":" + event.clientY);
    console.log(coordinates.top + " " + coordinates.bottom + " " + coordinates.left + " " + coordinates.right);*/

    const [pixelX, pixelY] = [event.clientX - coordinates.left, event.clientY - coordinates.top];
    console.log(pixelX, pixelY);

    /*canvas.width = outputImage.width;
    canvas.height = outputImage.height;
    context.drawImage(outputImage, 0, 0);*/
    const tempCanvas = document.createElement('canvas');
    tempCanvas.width = outputImage.width;
    tempCanvas.height = outputImage.height;
    const ctx = tempCanvas.getContext('2d');
    ctx.drawImage(outputImage, 0, 0, outputImage.width, outputImage.height);


    // Get the image data from the canvas    
    const imageData = ctx.getImageData(pixelX, pixelY, 1, 1);

    //const imageData = context.getImageData(pixelX, pixelY, 1, 1);
    const data = imageData.data; 
    console.log(data);    

    const r = data[0];
    const g = data[1];
    const b = data[2];

    const [h, s, l] = RGBToHSL(r, g, b);

    let hsl = [
        document.getElementById('h'),
        document.getElementById('s'),
        document.getElementById('l')
    ];   
    hsl[0].value = h;
    hsl[1].value = s;
    hsl[2].value = l;
       
    let Outputs = document.querySelectorAll('output');
    Outputs[0].value = h.toFixed();
    Outputs[1].value = s.toFixed();
    Outputs[2].value = l.toFixed();

    
    const[c, m, y, k] = rgb2cmyk(r, g, b, true);

    let cmyk = [
        document.getElementById('Cyan'),
        document.getElementById('Magenta'),
        document.getElementById('Yellow'),
        document.getElementById('Black'),
    ];
    cmyk[0].value = c;
    cmyk[1].value = m;
    cmyk[2].value = y;
    cmyk[3].value = k;

    Outputs[3].value = c.toFixed();
    Outputs[4].value = m.toFixed();
    Outputs[5].value = y.toFixed();
    Outputs[6].value = k.toFixed();
}

function CheckColorHSLforFirst(event) {
    //console.log(event.target.id);    
    const coordinates = previewImage.getBoundingClientRect();
    
    /*console.log("Координати: " + event.clientX + ":" + event.clientY);
    console.log(coordinates.top + " " + coordinates.bottom + " " + coordinates.left + " " + coordinates.right);*/

    const [pixelX, pixelY] = [event.clientX - coordinates.left, event.clientY - coordinates.top];
    console.log(pixelX, pixelY);

    const tempCanvas = document.createElement('canvas');
    tempCanvas.width = previewImage.width;
    tempCanvas.height = previewImage.height;
    const ctx = tempCanvas.getContext('2d');
    ctx.drawImage(previewImage, 0, 0, previewImage.width, previewImage.height);

    // Get the image data from the canvas    
    const imageData = ctx.getImageData(pixelX, pixelY, 1, 1);
    const data = imageData.data; 
    console.log(data);    

    const r = data[0];
    const g = data[1];
    const b = data[2];

    const [h, s, l] = RGBToHSL(r, g, b);

    let hsl = [
        document.getElementById('h'),
        document.getElementById('s'),
        document.getElementById('l')
    ];   
    hsl[0].value = h;
    hsl[1].value = s;
    hsl[2].value = l;
    
    let Outputs = document.querySelectorAll('output');
    Outputs[0].value = h.toFixed();
    Outputs[1].value = s.toFixed();
    Outputs[2].value = l.toFixed();


    const[c, m, y, k] = rgb2cmyk(r, g, b, true);

    let cmyk = [
        document.getElementById('Cyan'),
        document.getElementById('Magenta'),
        document.getElementById('Yellow'),
        document.getElementById('Black'),
    ];
    cmyk[0].value = c;
    cmyk[1].value = m;
    cmyk[2].value = y;
    cmyk[3].value = k;

    Outputs[3].value = c.toFixed();
    Outputs[4].value = m.toFixed();
    Outputs[5].value = y.toFixed();
    Outputs[6].value = k.toFixed();
}

var rgb2cmyk = function(r, g, b, normalized){
    var c = 1 - (r / 255);
    var m = 1 - (g / 255);
    var y = 1 - (b / 255);
    var k = Math.min(c, Math.min(m, y));
    
    c = (c - k) / (1 - k);
    m = (m - k) / (1 - k);
    y = (y - k) / (1 - k);
    
    if(!normalized){
        c = Math.round(c * 10000) / 100;
        m = Math.round(m * 10000) / 100;
        y = Math.round(y * 10000) / 100;
        k = Math.round(k * 10000) / 100;
    }
    
    c = isNaN(c) ? 0 : c;
    m = isNaN(m) ? 0 : m;
    y = isNaN(y) ? 0 : y;
    k = isNaN(k) ? 0 : k;
    
    return [c * 100, m * 100, y * 100, k * 100];
    /*return {
        c: c,
        m: m,
        y: y,
        k: k
    }*/
}

document.getElementById('download').addEventListener('click', () => {
    const link = document.createElement('a');
    link.href = outputImage.src;
    link.download = 'image'; 

    document.body.appendChild(link);
    link.click();
  
    document.body.removeChild(link);
});