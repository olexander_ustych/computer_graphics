const layout = {title: "Paeallelogram"};
/*const data = [{
    x:[-7, -7, 7, 7, -7],
    y:[-7, 7, 7, -7, -7],
}];
const plot = document.getElementById('plot');
Plotly.newPlot(plot, data, layout);*/


document.getElementById('start').addEventListener('click', () => {
    const firstSide = document.getElementById('firstSide').value;
    const secondSide = document.getElementById('secondSide').value;

    localStorage.setItem('firstSide', firstSide);
    localStorage.setItem('secondSide', secondSide);

    let angel = document.getElementById('angel').value;    
    if(firstSide == '' || secondSide == '')
    {
        alert("no");
        return;
    }
    if(angel == '')
    {
        angel = 0;
    }    
    else
    {
        document.getElementById('rightTurn').click();
        return;
    }
    angel = Math.PI * angel;
    localStorage.setItem('angel', angel);
    
    //firstSide = 18
    //secondSide = 10    
    const plot = document.getElementById('plot');
    Plotly.newPlot(plot);
    const offset = firstSide / 4;

    const xPoints = [
        firstSide / 2 * -1 - offset, 
        firstSide / 2 * -1 + offset, 
        firstSide / 2 + offset,
        firstSide / 2 - offset,

        firstSide / 2 * -1 - offset
    ];
    const yPoints = [
        secondSide / 2 * -1, 
        secondSide / 2, 
        secondSide / 2,
        secondSide / 2 * -1,

        secondSide / 2 * -1
    ];

    const data = [{
        x: xPoints,
        y: yPoints,
    }];

    Plotly.newPlot(plot, data, layout);
});










function buttonRotate(event) {
    let angel = document.getElementById('angel').value;
    if(angel == '')
    {
        return;
    }  
    //angel = parseFloat(Math.PI * angel);
    //const lastAngel = parseFloat(localStorage.getItem('angel')); 
    angel = parseFloat(Math.PI * angel);
    const lastAngel = parseFloat(localStorage.getItem('angel')); 

    if(event.target.id == 'leftTurn')
    {
        angel = lastAngel - angel;    
    }
    else if(event.target.id == 'rightTurn')
    {
        angel = lastAngel + angel;    
    }
    else {        
        if(event.target.parentElement.id == 'leftTurn')
        {
            angel = lastAngel - angel;    
        }
        else if(event.target.parentElement.id == 'rightTurn')
        {
            angel = lastAngel + angel;    
        }
        else
        {
            alert("?");
        }
    }
    localStorage.setItem('angel', angel);


    const firstSide = localStorage.getItem('firstSide');
    const secondSide = localStorage.getItem('secondSide');

    const offset = firstSide / 4;
    const xPoints = [
        firstSide / 2 * -1 - offset, 
        firstSide / 2 * -1 + offset, 
        firstSide / 2 + offset,
        firstSide / 2 - offset        
    ];
    const yPoints = [
        secondSide / 2 * -1, 
        secondSide / 2, 
        secondSide / 2,
        secondSide / 2 * -1        
    ];

    //матриця повороту
    /*
        за годинником
        [
            [cos a, -sin a],
            [sin a, cos a]
        ]

        проти годинника
        [
            [cos a, sin a],
            [-sin a, cos a]
        ]

        множення (приклад за годинником)
        [
            [x * cos A, y * (-1 * sin A)],
            +
            [x * sin A, y * cos A]
        ]
    */

    const M = [
        [Math.cos(angel), -1 * Math.sin(angel)],
        [Math.sin(angel), Math.cos(angel) ]
    ];
    
    let allPoints = [];
    for(let i = 0; i < xPoints.length; i++)
    {
        allPoints.push([xPoints[i], yPoints[i]]);
    }

    for(let i = 0; i < allPoints.length; i++)
    {
        allPoints[i] = [
            /*allPoints[i][0] * M[0][0] + allPoints[i][1] * M[1][0],
            allPoints[i][0] * M[0][1] + allPoints[i][1] * M[1][1]*/
            allPoints[i][0] * M[0][0] + allPoints[i][1] * M[0][1],
            allPoints[i][0] * M[1][0] + allPoints[i][1] * M[1][1]
        ];
    }

    for(let i = 0; i < allPoints.length; i++)
    {
        xPoints[i] = allPoints[i][0];
        yPoints[i] = allPoints[i][1];
    }
    xPoints.push(xPoints[0]);
    yPoints.push(yPoints[0]);


    const plot = document.getElementById('plot');
    Plotly.newPlot(plot);

    const data = [{
        x: xPoints,
        y: yPoints,
    }];

    Plotly.newPlot(plot, data, layout);
}
document.getElementById('leftTurn').addEventListener('click', buttonRotate);
document.getElementById('rightTurn').addEventListener('click', buttonRotate);


















































/*
function Complex(x, y) {
    this.x = x;
    this.y = y;
}

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

function MakeCoordinatePlate(scale) {
    const lineWidth = 5 * (1 / scale);

    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.beginPath();

    ctx.fillStyle = 'black';
    ctx.moveTo(canvas.width / 2 - lineWidth / 2, 0);
    ctx.lineTo(canvas.width / 2 - lineWidth / 2, canvas.height);
    ctx.lineTo(canvas.width / 2 + lineWidth / 2, canvas.height);
    ctx.lineTo(canvas.width / 2 + lineWidth / 2, 0);

    ctx.moveTo(0, canvas.height / 2 - lineWidth / 2);
    ctx.lineTo(canvas.width, canvas.height / 2 - lineWidth / 2);
    ctx.lineTo(canvas.width, canvas.height / 2 + lineWidth / 2);
    ctx.lineTo(0, canvas.height / 2 + lineWidth / 2);
    ctx.fill();

    ctx.fillRect(canvas.width / 2 - lineWidth / 2,
        canvas.height / 2 - lineWidth / 2, lineWidth, lineWidth);

    let step = canvas.width / 2 / 10 * (1 / scale);
    let stepIter = 0;
    for(let i = 0; i < 20 * scale; i++, stepIter += step)
    {
        if(i == 10 * scale)
        {
            continue;
        }

        ctx.moveTo(stepIter, canvas.height / 2 - 10);
        ctx.lineTo(stepIter + lineWidth, canvas.height / 2 - 10);
        ctx.lineTo(stepIter + lineWidth, canvas.height / 2 + 10);
        ctx.lineTo(stepIter, canvas.height / 2 + 10);
        ctx.fill();
    }
    stepIter = 0;
    for(let i = 0; i < 20 * scale; i++, stepIter += step)
    {
        if(i == 10 * scale)
        {
            continue;
        }

        ctx.moveTo(canvas.width / 2 - 10, stepIter);
        ctx.lineTo(canvas.width / 2 - 10, stepIter + lineWidth);
        ctx.lineTo(canvas.width / 2 + 10, stepIter + lineWidth);
        ctx.lineTo(canvas.width / 2 + 10, stepIter);
        ctx.fill();        
    }
}

function CreateParallelogram(firstSide, secondSide, angel, scale) {    
    localStorage.setItem('angel', angel);

    const lineWidth = 5 * (1 / scale);

    let step = canvas.width / 2 / 10 * (1 / scale);
    firstSide *= step;
    secondSide *= step;

    let firstPoint__left_top = new Complex(
        firstSide / 2 * (-1), secondSide / 2 * (-1)
    );
    let secondPoint__left_bottom = new Complex(
        firstSide / 2 * (-1), secondSide / 2
    );
    let thridPoint__right_top = new Complex(
        firstSide / 2, secondSide / 2 * (-1)
    );
    let fourthPoint__right_bottom = new Complex(
        firstSide / 2, secondSide / 2
    );

    let slide = firstSide / 6;
    firstPoint__left_top.x += slide;
    secondPoint__left_bottom.x -= slide;
    thridPoint__right_top.x += slide;
    fourthPoint__right_bottom.x -= slide;

    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(angel);

    ctx.beginPath();

    ctx.moveTo(firstPoint__left_top.x, firstPoint__left_top.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x, 
        thridPoint__right_top.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x, thridPoint__right_top.y - lineWidth / 2);
    ctx.lineTo(firstPoint__left_top.x, firstPoint__left_top.y - lineWidth / 2);
    ctx.fill();

    ctx.moveTo(firstPoint__left_top.x, firstPoint__left_top.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x + lineWidth, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.lineTo(firstPoint__left_top.x + lineWidth,
        firstPoint__left_top.y - lineWidth / 2)
    ctx.fill();

    ctx.moveTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y + lineWidth / 2)
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.fill();    

    ctx.moveTo(thridPoint__right_top.x, thridPoint__right_top.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x - lineWidth, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x - lineWidth ,
        thridPoint__right_top.y + lineWidth / 2);
    ctx.fill();     
    
    ctx.rotate(2 * Math.PI - angel);
    ctx.translate(canvas.width / 2 * (-1), canvas.height / 2 * (-1));
}

//MakeCoordinatePlate(1);

document.getElementById('start').addEventListener('click', () => {
    const firstSide = document.getElementById('firstSide').value;
    const secondSide = document.getElementById('secondSide').value;

    localStorage.setItem('firstSide', firstSide);
    localStorage.setItem('secondSide', secondSide);

    let angel = document.getElementById('angel').value;
    let scale = document.getElementById('scale').value;
    if(firstSide == '' || secondSide == '')
    {
        alert("no");
        return;
    }
    if(scale == '')
    {
        scale = 1;
    }
    if(angel == '')
    {
        angel = 0;
    }
    angel = Math.PI * angel;
    localStorage.setItem('angel', angel);
    MakeCoordinatePlate(scale);
    CreateParallelogram(firstSide, secondSide, angel, scale);
});

//CreateParallelogram(7, 5, 0, 1);
//CreateParallelogram(7, 5, Math.PI / 4, 1);
//CreateParallelogram(7, 5, Math.PI / 2, 1);

function buttonRotate(event) {
    let angel = document.getElementById('angel').value;
    if(angel == '')
    {
        return;
    }  
    angel = parseFloat(Math.PI * angel);
    const lastAngel = parseFloat(localStorage.getItem('angel')); 

    let scale = document.getElementById('scale').value;
    if(scale == '')
    {
        scale = 1;
    }

    MakeCoordinatePlate(scale);
    const firstSide = localStorage.getItem('firstSide');
    const secondSide = localStorage.getItem('secondSide');
    if(event.target.id == 'leftTurn')
    {
        CreateParallelogram(firstSide, secondSide, 
            lastAngel - angel, scale);
    }
    else if(event.target.id == 'rightTurn')
    {
        CreateParallelogram(firstSide, secondSide, 
            lastAngel + angel, scale);
    }
    else {        
        if(event.target.parentElement.id == 'leftTurn')
        {
            CreateParallelogram(firstSide, secondSide, 
                lastAngel - angel, scale);
        }
        else if(event.target.parentElement.id == 'rightTurn')
        {
            CreateParallelogram(firstSide, secondSide, 
                lastAngel + angel, scale);
        }
        else
        {
            alert("?");
        }
    }
}
document.getElementById('leftTurn').addEventListener('click', buttonRotate);
document.getElementById('rightTurn').addEventListener('click', buttonRotate);
*/





/*
// Define a rectangle
ctx.fillStyle = 'blue';
ctx.fillRect(50, 50, 100, 60);

// Save the unrotated context of the canvas
ctx.save();

// Rotate the canvas context by 45 degrees
ctx.translate(100, 100); // Adjust the pivot point for rotation (x, y)
ctx.rotate(Math.PI / 4); // 45 degrees in radians

// Draw the rectangle in the rotated context
ctx.fillStyle = 'red';
ctx.fillRect(-50, -30, 100, 60); // Centered rectangle at (0, 0) in the rotated context

// Restore the unrotated context for further drawing
ctx.restore();
*/







function Old_CreateParallelogram(firstSide, secondSide) {
    let scale = 1;
    const lineWidth = 5 * (1 / scale);

    let step = canvas.width / 2 / 10 * (1 / scale);
    firstSide *= step;
    secondSide *= step;

    let firstPoint__left_top = new Complex(
        canvas.width / 2 - firstSide / 2,
        canvas.height / 2 - secondSide / 2
    );
    let secondPoint__left_bottom = new Complex(
        canvas.width / 2 - firstSide / 2,
        canvas.height / 2 + secondSide / 2
    );
    let thridPoint__right_top = new Complex(
        canvas.width / 2 + firstSide / 2,
        canvas.height / 2 - secondSide / 2
    );
    let fourthPoint__right_bottom = new Complex(
        canvas.width / 2 + firstSide / 2,
        canvas.height / 2 + secondSide / 2
    );

    let slide = firstSide / 6;
    firstPoint__left_top.x += slide;
    secondPoint__left_bottom.x -= slide;
    thridPoint__right_top.x += slide;
    fourthPoint__right_bottom.x -= slide;

    ctx.beginPath();

    ctx.moveTo(firstPoint__left_top.x, firstPoint__left_top.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x, 
        thridPoint__right_top.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x, thridPoint__right_top.y - lineWidth / 2);
    ctx.lineTo(firstPoint__left_top.x, firstPoint__left_top.y - lineWidth / 2);
    ctx.fill();

    ctx.moveTo(firstPoint__left_top.x, firstPoint__left_top.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x + lineWidth, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.lineTo(firstPoint__left_top.x + lineWidth,
        firstPoint__left_top.y - lineWidth / 2)
    ctx.fill();

    ctx.moveTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y + lineWidth / 2)
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y - lineWidth / 2);
    ctx.lineTo(secondPoint__left_bottom.x, 
        secondPoint__left_bottom.y - lineWidth / 2);
    ctx.fill();    

    ctx.moveTo(thridPoint__right_top.x, thridPoint__right_top.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(fourthPoint__right_bottom.x - lineWidth, 
        fourthPoint__right_bottom.y + lineWidth / 2);
    ctx.lineTo(thridPoint__right_top.x - lineWidth ,
        thridPoint__right_top.y + lineWidth / 2);
    ctx.fill();    
}